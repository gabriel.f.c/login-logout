
import "./style.css"

function RestrictedPage({ isLoggedIn, user, login, logout }) {

    return (
        <div>
            <h1>{isLoggedIn ? `Bem-vindo, ${user}.` : `Você não pode acessar essa página.`}</h1>
            { isLoggedIn ?
            <button className="log__btn" onClick={ logout }>Logout</button> :
            <button className="log__btn" onClick={ login }>Login</button>
            }
        </div>
    )
}


export default RestrictedPage